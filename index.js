'use strict';

const Hapi		= require('hapi');
const server    = new Hapi.Server();

server.connection({
    port: 3000,
    host: 'localhost',
    address: '0.0.0.0'
});

server.route({
    method: 'GET',
    path: '/',
    handler: function(request, reply){
        reply('Hello world from Hapi.js');
    }
});

server.start(() => {
    console.log('Server starting at : ' + server.info.uri);
})